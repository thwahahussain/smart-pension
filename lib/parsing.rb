#
# @author thwaha.hussain
# This code reads the log file from arguement and parses the log file.
# gives the Output of most viewed url and most unique viewed url
#
class Parsing
   attr_reader :file_data, :file_path

   def initialize(args)
      if args.length != 1
         abort "Please mention the name of the file"
      end

      @file_path = args[0]
   end

   # This method is used to read the log file from arguement and stores it in instace variable
   # parameter -> file (filename)
   # 
   def read_file
      log_file_name = @file_path
      puts "Going to open '#{log_file_name}'"
      fh = open log_file_name

      file_hashed_data = {}
      while (line = fh.gets)
         url = line.split()[0]
         ip_address = line.split()[1]
         if file_hashed_data.has_key?(url)
            file_hashed_data[url].push(ip_address)
         else
            file_hashed_data[url] = [ip_address]
         end
      end
       
      fh.close
      @file_data = file_hashed_data
   end

   #This method is used to get the total count of the most viewed url
   def get_most_viewed_url(parsed_hash_data)
      puts "List of url with most page views ordered from most pages views to less page views \n"
      ((parsed_hash_data.map { |key,value| [key,value.length]
      }.to_h).sort_by {
         |key,value| value 
      }.reverse.to_h).each {
         |k,v|  puts "#{k}: #{v} visits"
      }
   end

   #This method is used to get the total count of most unique viewed url
   def get_unique_viewed_url(parsed_hash_data)
      puts "List of url with most unique page views ordered from most unique views to less unique views\n"
      ((parsed_hash_data.map {
         |key,value| [key,value.uniq.length]
      }.to_h).sort_by {
         |key, value| value
      }.reverse.to_h).each {
         |k,v|  puts "#{k}: #{v} unique views"
      }
   end
end

#This is used to run the script directly from the command line
if $PROGRAM_NAME == __FILE__
   parseData = Parsing.new(ARGV)
   parsed_hash_data = parseData.read_file
   parseData.get_most_viewed_url(parsed_hash_data)
   parseData.get_unique_viewed_url(parsed_hash_data)
end