require './spec/spec_helper'
require './lib/parsing'

RSpec.describe Parsing do
	subject(:read_file) { described_class.new(arguments).read_file }
	subject(:read_file_error) { described_class.new(empty_argument).read_file }
	subject(:get_most_viewed_url) { described_class.new(arguments).get_most_viewed_url(hash_data) }
	subject(:get_unique_viewed_url) { described_class.new(arguments).get_unique_viewed_url(hash_data) }

	let(:arguments) { ['lib/webserver.log'] }
	let(:empty_argument) { [] }
	let(:hash_data) do
    {
    	"/help_page/1"=>[
    		"126.318.035.038", "929.398.951.889", "722.247.931.582", "646.865.545.408", "543.910.244.929",
    		"016.464.657.359", "016.464.657.359", "929.398.951.889"
    	],
    	"/contact"=>[
    		"184.123.665.067", "184.123.665.067", "543.910.244.929", "555.576.836.194"
    	]
    }
  	end

	it 'Read the File' do
    	parsed_data = read_file
    	expect(parsed_data).not_to be_nil
    	expect(parsed_data.has_key?("/help_page/1")).to be_truthy
    	expect(parsed_data["/help_page/1"].length).to eq 80
  	end

  	it "When get most viewed url, get_most_viewed_url" do
  		parsed_data = get_most_viewed_url
  		expect(parsed_data).not_to be_nil
  		expect(parsed_data.has_key?("/contact")).to be_truthy
  		expect(parsed_data["/contact"]).to eq 4
  		expect(parsed_data["/help_page/1"]).to eq 8
  	end

  	it "When get unique viewed url, get_unique_viewed_url" do
  		parsed_data = get_unique_viewed_url
  		expect(parsed_data).not_to be_nil
  		expect(parsed_data.has_key?("/contact")).to be_truthy
  		expect(parsed_data["/contact"]).to eq 3
  		expect(parsed_data["/help_page/1"]).to eq 6
  	end

  	it 'Read empty File and catch abort error' do
    	expect(read_file_error).to output("Please mention the name of the file").to_stderr
  	end

end